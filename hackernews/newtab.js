// ==UserScript==
// @name         Hacker News New Tab
// @description  Open storylinks on Hacker News in a new tab
// @version      0.1
// @author       Marian Hähnlein <marian@haehnlein.at>
// @namespace    https://gitlab.com/HaehnleinMar/tampermonkey-scripts
// @supportURL   https://gitlab.com/HaehnleinMar/tampermonkey-scripts/issues
// @match        https://news.ycombinator.com/*
// @grant        none
// @license      MIT
// ==/UserScript==


(function() {
    'use strict';

    var links = document.getElementsByClassName("storylink");

    for (let link of links) {
        link.target = "_blank";
    };
})();
